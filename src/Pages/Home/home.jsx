import React from 'react'
import "./home.css"
import mapa from '../../utils/img/mapa.svg'
import title from '../../utils/img/title.png'
import cdl from '../../utils/img/cdl.svg'

export default (props) =>
    <div ref={props.referencia} className="home_container">
        <div className="content">
            <img src={mapa} alt="Franca" className="mapa" />
            <div className="logo-c">
                <img src={title} alt="CDL" className="w100" />
                <img src={cdl} alt="CDL" />
                <div className="btn-content">

                    <button onClick={props.clickParticipar} className="part-btn">
                        <p>QUERO PARTICIPAR!!</p>
                    </button>
                </div>
            </div>
        </div>
    </div>