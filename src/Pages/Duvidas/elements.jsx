// import React from 'react'
// import "./duvidas.css"
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// export default (props) =>
//     <React.Fragment>
//         <div className="duv-wrapper flex column">
//             <button
//                 onClick={
//                     props.numero === props.id ?
//                         props.clickUp :
//                         props.clickDown
//                 }
//                 className="duv-title flex row spa-bet">
//                 <p>{props.titulo}</p>
//                 <FontAwesomeIcon
//                     icon={props.numero === props.id ?
//                         "arrow-circle-up" :
//                         "arrow-circle-down"
//                     }
//                     style={{ fontSize: 52 }}
//                     color={'#0091ea'}
//                 />
//             </button>
//             <div style={
//                 props.numero === props.id ?
//                     {
//                         height: 'auto',
//                         paddingTop: 16,
//                         paddingRight: 10,
//                         paddingBottom: 10,
//                         paddingLeft: 10
//                     }
//                     :
//                     { height: 0, padding: 0 }
//             } className="duv-text">
//                 <p>{props.texto}</p>
//             </div>
//         </div>
//     </React.Fragment>
