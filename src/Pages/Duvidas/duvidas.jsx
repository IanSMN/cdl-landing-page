import React from 'react'
import './duvidas.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import face from '../../utils/img/face.png'
import insta from '../../utils/img/insta.png'

export default class Duvidas extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            duvidaSelecionada: null
        }
    }

    selecionar = (numero) => (
        this.setState({ duvidaSelecionada: numero })
    )

    render() {
        return (
            <div ref={this.props.referencia} className="duv-cont flex spa-start">

                <div className="title-cont">
                    <p className="title">DÚVIDAS</p>
                </div>

                <div className="duv-wrapper flex column ">
                    <button
                        onClick={() => {
                            this.state.duvidaSelecionada === "1" ?
                                this.selecionar(null) :
                                this.selecionar("1")
                        }}
                        className="duv-title flex row spa-bet"
                    >
                        <p>Primeira dúvida do bagulho?</p>
                        <FontAwesomeIcon
                            icon={this.state.duvidaSelecionada === "1" ? "arrow-circle-up" : "arrow-circle-down"}
                            style={{ fontSize: 52 }}
                            color={'#0091ea'}
                        />
                    </button>

                    <div
                        style={
                            this.state.duvidaSelecionada === "1" ?
                                {
                                    height: 'auto',
                                    paddingTop: 16,
                                    paddingRight: 10,
                                    paddingBottom: 10,
                                    paddingLeft: 10
                                } :
                                {
                                    height: 0, padding: 0
                                }
                        }
                        className="duv-text">
                        <p>
                            Como usuário: Os serviços de CDL Franca App estão disponíveis para pessoas físicas e
                            pessoas jurídicas regularmente inscritas nos cadastros de contribuintes federal e
                            estaduais que tenham capacidade legal para contratá-los. Não podem utilizá-los, assim,
                            pessoas que não gozem dessa capacidade, inclusive menores de 14 anos de idade ou
                            pessoas que tenham sido inabilitadas do CDL Franca App, temporária ou definitivamente.
                            Ficam, desde já, os Usuários advertidos das sanções legais cominadas no Código Civil. <br /> <br />
                            Como participantes “anunciantes” do app: pessoas jurídicas regularmente inscritas nos
                            cadastros de contribuintes federal e estaduais que tenham capacidade legal para contratá-los.
                        </p>
                    </div>

                </div>

                <div className="duv-wrapper flex column ">
                    <button
                        onClick={() => {
                            this.state.duvidaSelecionada === "2" ?
                                this.selecionar(null) :
                                this.selecionar("2")
                        }}
                        className="duv-title flex row spa-bet"
                    >
                        <p>Como adquirir pontuação e resgatar os prêmios?</p>
                        <FontAwesomeIcon
                            icon={this.state.duvidaSelecionada === "2" ? "arrow-circle-up" : "arrow-circle-down"}
                            style={{ fontSize: 52 }}
                            color={'#0091ea'}
                        />
                    </button>

                    <div
                        style={
                            this.state.duvidaSelecionada === "2" ?
                                {
                                    height: 'auto',
                                    paddingTop: 16,
                                    paddingRight: 10,
                                    paddingBottom: 10,
                                    paddingLeft: 10
                                } :
                                {
                                    height: 0, padding: 0
                                }
                        }
                        className="duv-text">
                        <p>
                            A pontuação será adquirida por meio da leitura do QR Code dentro dos
                            estabelecimentos participantes do app e por meio de compras realizadas nestes locais.
                            Será limitada a pontuação de um único ponto por visita diária por estabelecimento,
                            mas não há limite de pontuação por compra realizadas diariamente. <br />
                            <br /> - 1 ponto por leitura do QR code – Limitado a um visita diária por estabelecimento.
                            <br /> - 10 pontos por compra realizada independentemente do valor.
                           <br /> - O resgate dos pontos ocorreram ao termino de cada campanha de acordo com as datas
                                     estipulas no site e no aplicativo. Os prêmios serão entregues aos usuários com maior
                                     pontuação de cada campanha. Para a campanha final com a premiação do carro será
                                     considerado a somatória dos pontos adquiridos durante todo o ano.
                        </p>
                    </div>

                </div>

                <div className="duv-wrapper flex column ">
                    <button
                        onClick={() => {
                            this.state.duvidaSelecionada === "3" ?
                                this.selecionar(null) :
                                this.selecionar("3")
                        }}
                        className="duv-title flex row spa-bet"
                    >
                        <p>Quais as empresas participantes?</p>
                        <FontAwesomeIcon
                            icon={this.state.duvidaSelecionada === "3" ? "arrow-circle-up" : "arrow-circle-down"}
                            style={{ fontSize: 52 }}
                            color={'#0091ea'}
                        />
                    </button>

                    <div
                        style={
                            this.state.duvidaSelecionada === "3" ?
                                {
                                    height: 'auto',
                                    paddingTop: 16,
                                    paddingRight: 10,
                                    paddingBottom: 10,
                                    paddingLeft: 10
                                } :
                                {
                                    height: 0, padding: 0
                                }
                        }
                        className="duv-text">
                        <p>
                            No site CDLFranca App é possível conferir as empresas
                            que estão participando do app e das campanhas.
                        </p>
                    </div>

                </div>

                <div className="duv-wrapper flex column ">
                    <button
                        onClick={() => {
                            this.state.duvidaSelecionada === "4" ?
                                this.selecionar(null) :
                                this.selecionar("4")
                        }}
                        className="duv-title flex row spa-bet"
                    >
                        <p>Como faço para minha empresa participar do aplicativo e das promoções?</p>
                        <FontAwesomeIcon
                            icon={this.state.duvidaSelecionada === "4" ? "arrow-circle-up" : "arrow-circle-down"}
                            style={{ fontSize: 52 }}
                            color={'#0091ea'}
                        />
                    </button>

                    <div
                        style={
                            this.state.duvidaSelecionada === "4" ?
                                {
                                    height: 'auto',
                                    paddingTop: 16,
                                    paddingRight: 10,
                                    paddingBottom: 10,
                                    paddingLeft: 10
                                } :
                                {
                                    height: 0, padding: 0
                                }
                        }
                        className="duv-text">
                        <p>
                            Entre em contato pelo com um consultor CDL pelo telefone 3711-6300 ou
                            pelo site CDLFRANCAapp.com.br na abas promoções e faça seu cadastro.
                        </p>
                    </div>

                </div>

                <div
                    className="contate-nos"
                >
                    <div className="title-cont-2">
                        <p className="contate-title">CONTATE-NOS</p>
                    </div>
                    <div className="w100 flex row wrap spa-around">
                        <div className="w33 flex column spa-cent">
                            <h1>Email</h1>
                            <p style={{fontSize: 22, margin: 0}}>contatopromocao@cdl.com</p>
                        </div>
                        <div className="w33 flex column spa-cent">
                            <h1>Redes Sociais</h1>
                            <div className="flex row spa-bet" style={{ width: 100 }}>
                                <a href="" className="link">
                                    <img src={face} alt="" width={40} />
                                </a>
                                <a href="" className="link">
                                    <img src={insta} alt="" width={40} />
                                </a>
                            </div>
                        </div>
                        <div className="w33 flex column spa-cent">
                            <h1>Telefone</h1>
                            <p style={{fontSize: 26, margin: 0}}>(16) 3711-6300</p>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}