import React from 'react'
import "./regulamento.css"
import playstore from '../../utils/img/playstore.svg'
import apple from '../../utils/img/apple.svg'
import celular from '../../utils/img/cellphone.png'

export default (props) =>
    <div ref={props.referencia} className="reg-cont flex spa-around">

        <div className="title-cont">
            <p className="title">COMO PARTICIPAR?</p>
        </div>
        <div className="w-100 flex row spa-around wrap">
            <img src={celular} alt="tela CDL" className="img-phone" width={300} />
            <div style={{ width: 375 }}>
                <p className="desc">Baixe o CDL Caça ao Tesouro na Play Store ou na Loja Apple em seu celular e siga as instruções. </p>
            </div>
            <div style={{
                width: 150,
                height: 150,
                borderRadius: 16,
                backgroundColor: '#000'
            }}></div>
        </div>

        <div className="btn-cont flex wrap row spa-around">
            <button className="down-btn flex row spa-bet">
                <p>BAIXAR NA PLAY STORE</p>
                <div className="flex spa-cent" style={{ backgroundColor: '#f64e65' }}>
                    <img alt="logo" src={playstore} width={32} height={32} />
                </div>
            </button>
            <button className="down-btn flex row spa-bet">
                <p>BAIXAR NA LOJA APPLE</p>
                <div className="flex spa-cent" style={{ backgroundColor: '#212121' }}>
                    <img alt="logo" src={apple} width={38} height={38} />
                </div>
            </button>
        </div>

    </div>