import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './botaoMenu.css'

const botaoMenu = props =>
    <button className="button hamb" onClick={props.click}>
        <FontAwesomeIcon
            icon="bars"
            size={'2x'}
            color={'#233454'}
        />
    </button>

export default botaoMenu