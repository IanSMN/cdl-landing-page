import React from 'react';

import './menuLateral.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Logotipo from '../../utils/img/cdl-logotipo.png'
import ImagemBase from '../../utils/img/base-menu.jpg'

const menuLateral = props => {
    let drawerClasses = 'menu-drawer'
    if (props.show) {
        drawerClasses = 'menu-drawer open'
    }
    return (
        <nav className={drawerClasses}>
            <div className="logo-container p-20-20">
                <button className="back-btn" onClick={props.click}>
                    <FontAwesomeIcon
                        icon="arrow-left"
                        size={'2x'}
                        color={'#0091ea'}
                    />
                </button>
                <img src={Logotipo} alt="" style={{ marginLeft: 5, width: 96, height: 39 }} />
            </div>
            <div className="ml-content">
                <ul>
                    <li> <a href="/sobre">HOME</a> </li>
                    <li><a href="/servicos">REGULAMENTO</a> </li>
                    <li><a href="/">PREMIAÇÃO</a></li>
                    <li><a href="/seja-um-mantenedor">RANKING GERAL</a></li>
                    <li><a href="/equipe">RANKING POR ETAPA</a></li>
                    <li><a href="/entre-em-contato">CONTATE-NOS</a></li>
                    <li><a href="/">LOGIN</a></li>
                </ul>
                <img src={ImagemBase} alt="" className="img-base" />
            </div>
        </nav>
    )
}

export default menuLateral