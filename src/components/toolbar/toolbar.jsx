import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../../utils/img/cdl-logo.png'
import './toolbar.css'
import BotaoMenu from '../menuLateral/botaoMenu';

const Toolbar = (props) => (
    <header className="toolbar">
        <nav className="t-nav">
            <div className="logo">
                <div className="d-790">
                    <BotaoMenu click={props.menuClickHandler} />
                </div>
                <Link to="/">
                    <img src={logo} alt="" className="toolbar-img" />
                </Link>
            </div>
            <div className="t-nav-item">
                <ul>
                    <li>
                        <button onClick={props.toHome}><p>Home</p></button>
                    </li>
                    <li>
                        <button onClick={props.toRegulamento}><p>Participar</p></button>
                    </li>
                    <li>
                        <button onClick={props.toPremiacao}><p>Premiação</p></button>
                    </li>
                    <li>
                        <button onClick={props.toRankingGeral}><p>Ranking Geral</p></button>
                    </li>
                    <li>
                        <button onClick={props.toEtapa}><p>Ranking por Etapa</p></button>
                    </li>
                    <li>
                        <button onClick={props.toContate}><p>Dúvidas</p></button>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
)

export default Toolbar