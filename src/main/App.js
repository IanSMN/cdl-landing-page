import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {
  faBars,
  faArrowLeft,
  faArrowCircleDown,
  faArrowCircleUp
} from '@fortawesome/free-solid-svg-icons';

import Toolbar from '../components/toolbar/toolbar';
import Home from '../Pages/Home/home';
import Regulamento from '../Pages/Regulamento/regulamento';
import Rodape from '../rodape/rodape';
import Backdrop from '../components/backdrop/backdrop';
import MenuLateral from '../components/menuLateral/menuLateral';
import Duvidas from '../Pages/Duvidas/duvidas';
import RankingGeral from '../Pages/RankingGeral/RankingGeral';

library.add(
  fab,
  faBars,
  faArrowLeft,
  faArrowCircleDown,
  faArrowCircleUp
)

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      menuAberto: false
    }
  }

  scrollTo = (ref) => {
    window.scrollTo({ top: ref.offsetTop, behavior: 'smooth' })
  }

  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return { menuAberto: !prevState.menuAberto }
    })
  }

  backdropClickHandler = () => {
    this.setState({ menuAberto: false })
  }
  render() {
    let backdrop;

    if (this.state.menuAberto) {
      backdrop = <Backdrop click={this.backdropClickHandler} />
    }
    return (
      <BrowserRouter>
        <div>
          <Toolbar
            toHome={() => this.scrollTo(this.refHome)}
            toRegulamento={() => this.scrollTo(this.refRegulamento)}
            toRankingGeral={() => this.scrollTo(this.rankingGeral)}
            toContate={() => this.scrollTo(this.refDuvidas)}
            menuClickHandler={this.drawerToggleClickHandler}
          />
          <MenuLateral
            show={this.state.menuAberto}
            click={this.backdropClickHandler}
          />
          {backdrop}
          <main>
            <Home
              referencia={(ref) => this.refHome = ref}
              clickParticipar={() => this.scrollTo(this.refRegulamento)} />
            <Regulamento
              referencia={(ref) => this.refRegulamento = ref} />
            <RankingGeral
              referencia={(ref) => this.rankingGeral = ref} />
            <Duvidas
              referencia={(ref) => this.refDuvidas = ref}
            />
            <Rodape />
          </main>
        </div>
      </BrowserRouter >
    );
  }
}

export default App;
