import React from 'react';
import './rodape.css';
import Logo from '../utils/img/cdl-logo.png'
import LogoSmn from '../utils/img/logo-smn.svg'

export default () => (
    <div className="rodape">
        <div className="rodape-content w300">
            <div className="desc-content">
                <p className="p">Todos os direitos reservados</p>
                <p className="p">CDL FRANCA 2019</p>
            </div>
            <img src={Logo} className="rodape-img" alt="" />
        </div>
        <div className="rodape-content w230">
            <div className="desc-content">
                <p className="p">Desenvolvido por</p>
                <p className="p">SMN Informática 2019</p>
            </div>
            <img src={LogoSmn} style={{ height: 45 }} alt="" />
        </div>
    </div>
)